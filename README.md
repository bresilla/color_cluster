# COLOR CLUSTERING

example:

![lena.png](misc/lena.jpg)

two methods:
- using [OpenCV](https://opencv.org/) and HSV colorspace (8 clusters)
![lena.png](misc/lena_2.jpg)

- using [DIPlib](https://diplib.org/) and Lab colorspace (8 clusters)
![lena.png](misc/lena_3.jpg)