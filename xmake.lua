set_languages("cxx20")
add_rules("mode.debug", "mode.release")

set_optimize("fastest")
-- add_includedirs("/opt/libtorch/include", "/usr/local/include", "/usr/include")
-- add_linkdirs("/opt/libtorch/lib", "/usr/local/lib", "/usr/lib")
add_cxflags("-fno-strict-aliasing", "-DDEBUG")
-- add_ldflags("-fuse-ld=mold", "-lpthread", {force = true})

add_requires("opencv")
add_requires("fmt")

target("classy")
    set_kind("binary")
    add_files("src/*.cpp")
    add_packages("opencv", "fmt")
